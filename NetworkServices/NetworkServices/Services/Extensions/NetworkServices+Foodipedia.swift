//
// Created by Rafał on 02/08/2017.
// Copyright (c) 2017 Rafał. All rights reserved.
//

import Foundation
import LifesumApi
import RxSwift

extension NetworkServices: FoodipediaAPI {
    public func foodipedia(for foodId: String) -> Observable<FoodipediaResponse> {
        let pathUrl = self.pathUrl() + "codetest"

        let parameters = ["foodid": foodId]

        let observer: Observable<FoodipediaResponse> = NetworkServices().request(
                urlPath: pathUrl,
                withParameters: parameters,
                methodType: .get)

        return observer
    }
}
