//
// Created by Rafał on 31/07/2017.
// Copyright (c) 2017 Rafał. All rights reserved.
//

import Foundation

public struct NetworkServiceModel {
    public var apiUrl: String? = nil
    public var prefixUrl: String? = nil
    public var siteUrl: String? = nil

    public init() {

    }

    public init(apiUrl: String?, prefixUrl: String?, siteUrl: String?) {
        self.apiUrl = apiUrl
        self.prefixUrl = prefixUrl
        self.siteUrl = siteUrl
    }

    public func pathUrlWithNoPrefix() -> String {
        guard let apiUrl = apiUrl, let siteUrl = siteUrl else {
            return ""
        }

        return apiUrl + siteUrl
    }

    public func pathUrl() -> String {
        guard let apiUrl = apiUrl, let prefixUrl = prefixUrl, let siteUrl = siteUrl else {
            return ""
        }

        return apiUrl + prefixUrl + siteUrl
    }
}