//
// Created by Rafał on 31/07/2017.
// Copyright (c) 2017 Rafał. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import RxSwift
import PKHUD

public protocol NetworkServiceProtocol {
    func request<T: BaseMappable>(
        urlPath: String,
        withParameters: Dictionary<String, Any>?,
        methodType: Alamofire.HTTPMethod) -> Observable<T>
}

public class NetworkServices: NetworkServiceProtocol {

    // This mode will be used to stored setting in App Setting.
    // Currently i just set up this model programmatically.
    private(set) var serviceModel: NetworkServiceModel? = NetworkServiceModel(
            apiUrl: "https://api.lifesum.com/",
            prefixUrl: "v2/",
            siteUrl: "foodipedia/")

    public init() {
        let sessionManager = SessionManager.default
        sessionManager.adapter = AccessTokenAdapter(accessToken: "732164:ff93bdcb783b5e1d14a236dd13339096c68630dfbf04f5db8204c4e34d9f8b91")
    }

    public func request<T: BaseMappable>(
            urlPath: String,
            withParameters: Dictionary<String, Any>?,
            methodType: Alamofire.HTTPMethod) -> Observable<T> {

        let observable: Observable<T> = Observable.create({
            observer in

            let request = Alamofire.request(
                            urlPath,
                            method: methodType,
                            parameters: withParameters,
                            encoding: methodType == .get ? URLEncoding.default : JSONEncoding.default)
                    .responseString {
                        response in

                        if let error = response.error {
                            observer.onError(error)
                        } else {
                            if let JSON = response.result.value {
                                if let object = Mapper<T>().map(JSONString: JSON) {
                                    observer.onNext(object)
                                    observer.onCompleted()
                                } else {
                                    observer.onError(NSError(domain: "JSON mapping Error", code: -1, userInfo: nil))
                                }
                            }
                        }
                    }

            return Disposables.create {
                request.cancel()
            }
        })

        return observable
    }

    public func configureServices(apiUrl: String, prefixUrl: String, siteUrl: String) {
        serviceModel = NetworkServiceModel(apiUrl: apiUrl, prefixUrl: prefixUrl, siteUrl: siteUrl)
    }

    public func configureService(serviceModel: NetworkServiceModel?) {
        if let serviceModel = serviceModel {
            self.serviceModel = serviceModel
        }
    }

    public func pathUrlWithNoPrefix() -> String {
        if let serviceModel = serviceModel {
            return serviceModel.pathUrlWithNoPrefix()
        }

        return ""
    }

    public func pathUrl() -> String {
        if let serviceModel = serviceModel {
            return serviceModel.pathUrl()
        }

        return ""
    }
}

class AccessTokenAdapter: RequestAdapter {
    private let accessToken: String

    init(accessToken: String) {
        self.accessToken = accessToken
    }

    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var urlRequest = urlRequest

        if let url = urlRequest.url {
            if url.absoluteString.hasPrefix(NetworkServices().pathUrl()) {
                urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
                urlRequest.setValue(accessToken, forHTTPHeaderField: "Authorization")
            }
        }

        return urlRequest
    }
}
