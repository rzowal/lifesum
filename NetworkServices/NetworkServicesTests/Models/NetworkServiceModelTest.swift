//
//  NetworkServiceModelTest.swift
//  NetworkServices
//
//  Created by Rafał on 02/08/2017.
//  Copyright © 2017 Rafał. All rights reserved.
//

import XCTest
import Nimble

@testable import NetworkServices

class NetworkServiceModelTest: XCTestCase {
    var networkModel: NetworkServiceModel = NetworkServiceModel(
            apiUrl: "https://api.lifesum.com/",
            prefixUrl: "v2/",
            siteUrl: "foodipedia/")

    func testNetworkServiceModelPathUrl() {
        let pathExpectationResult = "https://api.lifesum.com/v2/foodipedia/"
        let pathUrl = networkModel.pathUrl()

        expect(pathUrl)
                .to(equal(pathExpectationResult))
    }

    func testNetworkServiceModelPathUrlWithNoPrefix() {
        let pathExpectationResult = "https://api.lifesum.com/foodipedia/"
        let pathUrl = networkModel.pathUrlWithNoPrefix()

        expect(pathUrl)
                .to(equal(pathExpectationResult))
    }
}
