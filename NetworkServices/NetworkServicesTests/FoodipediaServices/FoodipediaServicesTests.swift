//
//  FoodipediaServices.swift
//  NetworkServices
//
//  Created by Rafał on 31/07/2017.
//  Copyright © 2017 Rafał. All rights reserved.
//

import XCTest

import Nimble
import Quick
import RxSwift
import RxBlocking
import LifesumStubWoofer

@testable import NetworkServices

class FoodipediaServicesTests: XCTestCase {

    var disposeBag = DisposeBag()

    override func tearDown() {
        StubWoofer.removeAll()
        super.tearDown()
    }

    func testFoodipediaFirstFoodValid() {

        // Example test using Nimble and StubWoofer to read JSONFile on respond
        // We using here RxBlocking for wait for response
        let networkServices = NetworkServices()
        StubWoofer.stubFoodipedia(response: .validFirstFood)

        do {
            let results = try networkServices
                    .foodipedia(for: "1")
                    .toBlocking()
                    .single()

            expect(results?.response.title).to(equal("Ricotta cheese"))

        } catch {
            fail("Should never happen")
        }
    }

    func testFoodipediaLastFoodValid() {

        // Example test - using Nimble for waiting for response
        // But Mostly i prefer to use Quick and Nimble test fto that we will have good
        // DECLARATIVE PROGRAMMING
        let networkService = NetworkServices()
        StubWoofer.stubFoodipedia(response: .validFirstFood)

        waitUntil {
            done in
            networkService
                    .foodipedia(for: "200")
                    .asObservable()
                    .subscribe {
                        event in
                        switch event {
                        case .next:
                            done()
                        case .completed:
                            break
                        case .error(let error):
                            fail("Error should't be called. Error \(error)")
                        }

                    }
                    .addDisposableTo(self.disposeBag)
        }
    }

    func testFoodipediaInvalidResponse() {
        let networkServices = NetworkServices()
        StubWoofer.stubFoodipedia(response: .invalidJSON)

        do {
            _ = try networkServices
                    .foodipedia(for: "1")
                    .toBlocking()
                    .single()

            fail("Expected error")

        } catch {

        }
    }
}
