//
// Created by Rafał on 31/07/2017.
// Copyright (c) 2017 Rafał. All rights reserved.
//

import Foundation
import RxSwift

public protocol FoodipediaAPI {
    func foodipedia(for foodId: String) -> Observable<FoodipediaResponse>
}
