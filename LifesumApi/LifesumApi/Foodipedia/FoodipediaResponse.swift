//
// Created by Rafał on 31/07/2017.
// Copyright (c) 2017 Rafał. All rights reserved.
//

import Foundation
import ObjectMapper

public struct Meta: Mappable {
    public var code: Int = 0

    public init() { }
    public init?(map: Map) { }

    public mutating func mapping(map: Map) {
        code <- map["code"]
    }
}

public struct Response: Mappable {
    public var carbs: Float = 0.0
    public var fiber: Float = 0
    public var title: String = ""
    public var pcstext: String = ""
    public var potassium: Float = 0.0
    public var sodium: Float = 0.0
    public var calories: Int = 0
    public var fat: Float = 0.0
    public var sugar: Float = 0.0
    public var gramsperserving: Int = 0
    public var cholesterol: Float = 0
    public var protein: Float = 0
    public var unsaturatedfat: Float = 0
    public var saturatedfat: Float = 0

    public init() { }
    public init?(map: Map) { }

    public mutating func mapping(map: Map) {
        carbs <- map["carbs"]
        fiber <- map["fiber"]
        title <- map["title"]
        pcstext <- map["pcstext"]
        potassium <- map["potassium"]
        sodium <- map["sodium"]
        calories <- map["calories"]
        fat <- map["fat"]
        sugar <- map["sugar"]
        gramsperserving <- map["gramsperserving"]
        cholesterol <- map["cholesterol"]
        protein <- map["protein"]
        unsaturatedfat <- map["unsaturatedfat"]
        saturatedfat <- map["saturatedfat"]
    }
}

public struct FoodipediaResponse: Mappable {

    public var meta = Meta()
    public var response = Response()

    public init() { }
    public init?(map: Map) { }

    public mutating func mapping(map: Map) {
        meta <- map["meta"]
        response <- map["response"]
    }

    public mutating func convertToTemplateResponse() {
        meta.code = 200

        response.carbs = 3.04
        response.fiber = 0
        response.title = "Ricotta cheese"
        response.pcstext = "Whole cheese"
        response.potassium = 0.105
        response.sodium = 0.084
        response.calories = 174
        response.fat = 12.98
        response.sugar = 0.27
        response.gramsperserving = 20
        response.cholesterol = 0.051
        response.protein = 11.26
        response.unsaturatedfat = 4.012
        response.saturatedfat = 8.295
    }
}
