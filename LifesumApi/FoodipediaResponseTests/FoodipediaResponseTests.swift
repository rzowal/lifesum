//
//  FoodipediaResponseTests.swift
//  FoodipediaResponseTests
//
//  Created by Rafał on 02/08/2017.
//  Copyright © 2017 Rafał. All rights reserved.
//

import XCTest
import Nimble
import LifesumStubWoofer

@testable import LifesumApi

class FoodipediaResponseTests: XCTestCase {
    func testFoodipediaRespondComplete() {
        let respondFileName = "ValidFirstFood"

        guard let object: FoodipediaResponse = StubWoofer.loadJSON(name: respondFileName) else {
            fail("Can't load \(respondFileName)")
            return
        }

        expect(object.meta.code).to(equal(200))

        expect(object.response.carbs).to(equal(3.04))
        expect(object.response.fiber).to(equal(0))
        expect(object.response.title).to(equal("Ricotta cheese"))
        expect(object.response.pcstext).to(equal("Whole cheese"))
        expect(object.response.potassium).to(equal(0.105))
        expect(object.response.sodium).to(equal(0.084))
        expect(object.response.calories).to(equal(174))
        expect(object.response.fat).to(equal(12.98))
        expect(object.response.sugar).to(equal(0.27))
        expect(object.response.gramsperserving).to(equal(20))
        expect(object.response.cholesterol).to(equal(0.051))
        expect(object.response.protein).to(equal(11.26))
        expect(object.response.unsaturatedfat).to(equal(4.012))
        expect(object.response.saturatedfat).to(equal(8.295))

        let responseObject = FoodipediaResponse()
        expect(responseObject).toNot(beNil())
    }
}
