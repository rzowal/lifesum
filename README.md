# lifesum
Lifesim Code Test is an exciting new code test for ios Developer 


# Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Prerequisites

### Xcode.
First you need to install the latest Xcode version, you can find it here:
Tip:  for Developing i propouse to use AppCode  https://www.jetbrains.com/objc/

```
https://developer.apple.com/download/more/
```
Don't install from Mac App Store, to avoid autoupdater issues.

### Homebrew
Install Homebrew:
```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

### Ruby via RVM
Install Ruby Environment Manager (RVM). To install RVM write in Terminal command line:

```
\curl -sSL https://get.rvm.io | bash -s stable --rails
```

It takes a while. After installation open new Terminal window to confirm changes.
Now you have to install gems. To do that, write:

### Fastlane
```
gem install fastlane
gem install slather
```

#### Carthage
The next installation is Homebrew software, write:

```
brew install carthage
```

#### Swiftlint

```
brew install swiftlint
```

### PIP
```
[sudo] easy_install pip
```

#### lizard
```
[sudo] pip install lizard
```

# fastlane

[Fastlane actions](fastlane/README.md)

