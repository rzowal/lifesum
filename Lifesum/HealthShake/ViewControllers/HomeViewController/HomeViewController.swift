//
//  HomeViewController.swift
//  HealthShake
//
//  Created by Rafał on 02/08/2017.
//  Copyright © 2017 Rafał. All rights reserved.
//

import Foundation
import UIKit
import Hero
import NetworkServices

class HomeViewController: UIViewController {
    
    // MARK: Accessability
    
    enum AccessibilityID: String {
        case startButton
        case view
        
        var value: String {
            return String(describing: HomeViewController.self) + "_" + self.rawValue
        }
    }

    // Outlet
    @IBOutlet weak var startButton: UIButton!

    // MARK: Stups view
    override func viewDidLoad() {
        super.viewDidLoad()

        configureController()
        setupButton()
    }

    private func configureController() {
        self.view.accessibilityIdentifier = AccessibilityID.view.value
    }
    
    private func setupButton() {
        startButton.accessibilityIdentifier = AccessibilityID.startButton.value

        Roundlayer.setupGradientView(view: startButton,
        gradientColors: [Roundlayer.cgColorForRed(red: 243.0, green: 167.0, blue: 78.0),
        Roundlayer.cgColorForRed(red: 237.0, green: 84.0, blue: 96.0)],
        direction: [CGPoint(x: 0, y: 1), CGPoint(x: 1, y: 1)],
        radius: 37)
    }

    // MARK : Actions
    @IBAction func showFoodShakeView() {
        let vc = viewController(forStoryboardName: "Foodipedia")

        // iOS bug: https://github.com/lkzhao/Hero/issues/106 https://github.com/lkzhao/Hero/issues/79
        DispatchQueue.main.async {
            self.present(vc, animated: true, completion: nil)
        }
    }
}
