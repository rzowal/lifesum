//
// Created by Rafał on 03/08/2017.
// Copyright (c) 2017 Rafał. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import LifesumApi
import NetworkServices

// MARK: FoodipediaInputs

protocol FoodipediaInputs {
    func randomFood()
}

// MARK : FoodipediaOutputs

protocol FoodipediaOutputs {
    var onResultFoodipediaRequest: Driver<Result<FoodipediaResponse>> { get }
    var onRequestProcess: Driver<Bool> { get }
}

// MARK: FoodipediaViewMotelType

protocol FoodipediaViewModelType {
    var inputs: FoodipediaInputs { get }
    var outputs: FoodipediaOutputs { get }
}

// MARK: FoodipediaViewModel

final class FoodipediaViewModel: FoodipediaViewModelType {

    fileprivate let networkServices: NetworkServices
    // Inputs
    fileprivate let valueFoodId = Variable<String>("")
    fileprivate let signalFoodRequest = PublishSubject<Void>()

    // Outputs
    fileprivate var driverResultFoodipedia: Driver<Result<FoodipediaResponse>> = .empty()

    // Internal
    fileprivate let probeFoodRequest = ObservableProbe()

    // RandomFoodViewModelType
    var inputs: FoodipediaInputs {
        return self
    }
    var outputs: FoodipediaOutputs {
        return self
    }

    // MARK: Init - Designated
    init(networkServices: NetworkServices) {

        self.networkServices = networkServices

        // Make driver for Foodipedia request
        let probe = probeFoodRequest
        let tryFoodipedia = signalFoodRequest
                .asObservable()
                .withLatestFrom(valueFoodId.asObservable())
                .flatMapLatest { (foodId) -> Observable<Result<FoodipediaResponse>> in
                    return networkServices
                            .foodipedia(for: foodId)
                            .trackActivity(probe)
                            .asResult()
                }

        self.driverResultFoodipedia = tryFoodipedia.asDriver(onErrorDriveWith: Driver.never())
    }
}

// MARK: FoodipediaInputs

extension FoodipediaViewModel: FoodipediaInputs {
    func randomFood() {
        let generator = Int(arc4random_uniform(200))
        valueFoodId.value = String(generator)
        signalFoodRequest.onNext()
    }
}

// MARK: FoodipediaOutputs

extension FoodipediaViewModel: FoodipediaOutputs {
    var onResultFoodipediaRequest: Driver<Result<FoodipediaResponse>> {
        return driverResultFoodipedia.asDriver()
    }

    var onRequestProcess: Driver<Bool> {
        return probeFoodRequest.asDriver()
    }
}
