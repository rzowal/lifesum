//
// Created by Rafał on 03/08/2017.
// Copyright (c) 2017 Rafał. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import UIKit
import NetworkServices
import LifesumApi

// MARK: FoodipediaViewController

final class FoodipediaViewController: UIViewController {

    // MARK: Accessability

    enum AccessibilityID: String {
        case moreInfoButton
        case view

        var value: String {
            return String(describing: FoodipediaViewController.self) + "_" + self.rawValue
        }
    }

    // Const

    public struct Const {
        static let moreInfoSegueIdentifier = "moreInfoSegueIdentifier"
    }

    // Outlets
    @IBOutlet weak var circleLoading: CircleLoading!

    @IBOutlet weak var circleView: UIView!
    @IBOutlet weak var foodName: UILabel!
    @IBOutlet weak var caloriesValueLabel: UILabel!

    @IBOutlet weak var carbsValueLabel: UILabel!
    @IBOutlet weak var proteinValueLabel: UILabel!
    @IBOutlet weak var fatValueLabel: UILabel!

    @IBOutlet weak var moreInfoButton: UIButton!

    // View Model
    fileprivate var viewModel: FoodipediaViewModelType!

    // Rx
    fileprivate var disposeBag = DisposeBag()

    // Internal
    fileprivate var foodipediaResponse: FoodipediaResponse?

    // MARK: View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Prepare View Model before setup controls kick off
        setupViewModel()
        setupBindings()

        // Setup all controls
        configureController()
        circleLoading.stop()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        super.becomeFirstResponder()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.resignFirstResponder()
        super.viewWillDisappear(animated)
    }

    override var canBecomeFirstResponder: Bool {
        return true
    }

    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            viewModel.inputs.randomFood()
        }
    }
}

extension FoodipediaViewController {

    fileprivate func setupViewModel() {
        let vm = FoodipediaViewModel(networkServices: NetworkServices())
        self.viewModel = vm
    }

    fileprivate func setupBindings() {
        viewModel.outputs.onResultFoodipediaRequest
                .drive(onNext: { [weak self] (result) in
                    switch result {
                    case .success(let resultResources):
                        self?.setupOutlets(result: resultResources)
                    case .error:
                        self?.circleLoading.stop()
                        break
                    }
                })
                .addDisposableTo(disposeBag)

        viewModel.outputs.onRequestProcess
                .drive(onNext: {
                    [weak self]
                    progres in
                    self?.set(inProgressMode: progres)
                })
                .addDisposableTo(disposeBag)
    }

    fileprivate func set(inProgressMode: Bool) {
        if inProgressMode {
            circleLoading.start()
        } else {
            circleLoading.stop()
        }
    }

    private func setupOutlets(result: FoodipediaResponse) {
        foodipediaResponse = result

        foodName.text = result.response.title.uppercased()
        caloriesValueLabel.text = String(result.response.calories)
        carbsValueLabel.text = "\(Int(result.response.carbs))%"
        proteinValueLabel.text = "\(Int(result.response.protein))%"
        fatValueLabel.text = "\(Int(result.response.fat))%"
    }
}

extension FoodipediaViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if Const.moreInfoSegueIdentifier == segue.identifier {
            let vc = segue.destination as! MoreInfoViewController

            if let foodipediaResponse = foodipediaResponse {
                vc.configureController(foodipediaResponse: foodipediaResponse)
            }
        }
    }
}

extension FoodipediaViewController {

    fileprivate func configureController() {
        self.view.accessibilityIdentifier = AccessibilityID.view.value

        foodipediaResponse = FoodipediaResponse()
        foodipediaResponse?.convertToTemplateResponse()

        setupCircleView()
        setupMoreInfoButton()
    }

    func setupCircleView() {

        Roundlayer.setupGradientView(
                view: circleView,
                gradientColors: [Roundlayer.cgColorForRed(red: 243.0, green: 167.0, blue: 78.0),
                                 Roundlayer.cgColorForRed(red: 237.0, green: 84.0, blue: 96.0)],
                direction: [CGPoint(x: 0, y: 1), CGPoint(x: 1, y: 1)],
                radius: circleView.frame.size.width / 2)
    }

    func setupMoreInfoButton() {
        moreInfoButton.accessibilityIdentifier = AccessibilityID.moreInfoButton.value
        Roundlayer.setupGradientView(
                view: moreInfoButton,
                gradientColors: [Roundlayer.cgColorForRed(red: 1.0, green: 5.0, blue: 33.0),
                                 Roundlayer.cgColorForRed(red: 2.0, green: 6.0, blue: 39.0)],
                direction: [CGPoint(x: 0, y: 1), CGPoint(x: 1, y: 1)],
                radius: moreInfoButton.frame.size.height / 2)
    }
}
