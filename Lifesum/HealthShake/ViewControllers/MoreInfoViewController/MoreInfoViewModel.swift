//
// Created by Rafał on 04/08/2017.
// Copyright (c) 2017 Rafał. All rights reserved.
//

import Foundation
import RxSwift
import LifesumApi

// MARK : MoreInfoOutputs

protocol MoreInfoOutputs {
    var outDataSource: BehaviorSubject<(UICollectionViewDataSource & UICollectionViewDelegate)?> { get }
    var outReloadTableView: Observable<Void> { get }
}

// MARK: MoreInfoVieModelViewMotelType

protocol MoreInfoVieModelType {
    var outputs: MoreInfoOutputs { get }
}

// MARK: MoreInfoVieModel

class MoreInfoViewModel: MoreInfoVieModelType {

    // Outputs
    var subjectDataSource = BehaviorSubject<(UICollectionViewDataSource & UICollectionViewDelegate)?>(value: nil)
    let subjectReloadTableView = PublishSubject<Void>()

    // Internal
    let disposeBag = DisposeBag()
    var selectedProduct: FoodipediaResponse

    var outputs: MoreInfoOutputs {
        return self
    }

    // MARK: Init - Designated
    init(selectedProduct: FoodipediaResponse) {

        self.selectedProduct = selectedProduct

        let cell1 = MoreInfoCellViewModel(
                title: selectedProduct.response.title.uppercased(),
                firstParameter: String(selectedProduct.response.calories),
                firstDescription: "cals / serving",
                secondParameter: "\(Int(selectedProduct.response.unsaturatedfat))%",
                secondDescription: "of fat is unsaturated",
                style: MoreInfoCellStyle(
                        gradientColours: Roundlayer.GradientColour.orange,
                        backgroundColour: Roundlayer.GradientColour.backgroundOrange))

        let cell2 = MoreInfoCellViewModel(
                title: selectedProduct.response.title.uppercased(),
                firstParameter: String(selectedProduct.response.fat),
                firstDescription: "fat in that food",
                secondParameter: "\(Int(selectedProduct.response.saturatedfat))%",
                secondDescription: "of fat is saturated",
                style: MoreInfoCellStyle(
                        gradientColours: Roundlayer.GradientColour.green,
                        backgroundColour: Roundlayer.GradientColour.backgroundGreen))

        let cell3 = MoreInfoCellViewModel(
                title: selectedProduct.response.title.uppercased(),
                firstParameter: String(selectedProduct.response.potassium),
                firstDescription: "potasium / serving",
                secondParameter: "\(Int(selectedProduct.response.sodium))%",
                secondDescription: "of fat is sodium",
                style: MoreInfoCellStyle(
                        gradientColours: Roundlayer.GradientColour.purple,
                        backgroundColour: Roundlayer.GradientColour.backgroundPurple))

        let cell4 = MoreInfoCellViewModel(
                title: selectedProduct.response.title.uppercased(),
                firstParameter: String(selectedProduct.response.gramsperserving),
                firstDescription: "gramsperserving / serving",
                secondParameter: "\(Int(selectedProduct.response.sugar))%",
                secondDescription: "of fat is sugar",
                style: MoreInfoCellStyle(
                        gradientColours: Roundlayer.GradientColour.green,
                        backgroundColour: Roundlayer.GradientColour.backgroundGreen))

        let cell5 = MoreInfoCellViewModel(
                title: selectedProduct.response.title.uppercased(),
                firstParameter: String(selectedProduct.response.protein),
                firstDescription: "protein / servin",
                secondParameter: "\(Int(selectedProduct.response.cholesterol))%",
                secondDescription: "of fat is cholesterol",
                style: MoreInfoCellStyle(
                        gradientColours: Roundlayer.GradientColour.purple,
                        backgroundColour: Roundlayer.GradientColour.backgroundPurple))

        let cell6 = MoreInfoCellViewModel(
                title: selectedProduct.response.title.uppercased(),
                firstParameter: String(selectedProduct.response.carbs),
                firstDescription: "carbs / servin",
                secondParameter: "\(Int(selectedProduct.response.fiber))%",
                secondDescription: "of fat is fiber",
                style: MoreInfoCellStyle(
                        gradientColours: Roundlayer.GradientColour.orange,
                        backgroundColour: Roundlayer.GradientColour.backgroundPurple))

        let cellRows: [MoreInfoCellViewModel] = [
                cell1, cell2, cell3, cell4, cell5, cell6
        ]

        let dataSource = MoreInfoDataSource(data: cellRows)

        self.publish(dataSource: dataSource)
    }

    func publish(dataSource: (UICollectionViewDataSource & UICollectionViewDelegate)) {

        self.subjectDataSource.on(.next(dataSource))
    }
}

// MARK: MoreInfoOutputs

extension MoreInfoViewModel: MoreInfoOutputs {
    var outDataSource: BehaviorSubject<(UICollectionViewDataSource & UICollectionViewDelegate)?> {
        return subjectDataSource
    }

    var outReloadTableView: Observable<Void> {
        return subjectReloadTableView
    }
}
