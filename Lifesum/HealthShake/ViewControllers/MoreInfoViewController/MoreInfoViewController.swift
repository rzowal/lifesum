//
// Created by Rafał on 04/08/2017.
// Copyright (c) 2017 Rafał. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import LifesumApi

final class MoreInfoViewController: UIViewController {

    // MARK: Accessability

    enum AccessibilityID: String {
        case view

        var value: String {
            return String(describing: FoodipediaViewController.self) + "_" + self.rawValue
        }
    }

    // Outlets
    @IBOutlet weak var foodNameLabel: UILabel!
    @IBOutlet weak var moreInfoCollectionView: UICollectionView!

    // Internal
    fileprivate var foodipediaResponse: FoodipediaResponse!

    // ViewModel
    fileprivate var viewModel: MoreInfoVieModelType!

    // Rx
    fileprivate var disposeBag = DisposeBag()

    // MARK: Const

    struct Const {
        static let MoreInfoCellIdentifier = "MoreInfoCell"
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        configureController()

        foodNameLabel.text = foodipediaResponse.response.title.uppercased()

        viewModel = MoreInfoViewModel(selectedProduct: foodipediaResponse)

        viewModel?
                .outputs
                .outDataSource
                .subscribe(onNext: {
                    [unowned self]
                    source in
                    self.moreInfoCollectionView.dataSource = source
                    self.moreInfoCollectionView.delegate = source
                    self.moreInfoCollectionView.reloadData()
                }).addDisposableTo(disposeBag)
    }

    fileprivate func configureController() {
        self.view.accessibilityIdentifier = AccessibilityID.view.value
    }

    public func configureController(foodipediaResponse: FoodipediaResponse) {
        self.foodipediaResponse = foodipediaResponse
    }
}
