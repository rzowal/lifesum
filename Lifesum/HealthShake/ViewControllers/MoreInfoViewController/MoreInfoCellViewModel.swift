//
// Created by Rafał on 04/08/2017.
// Copyright (c) 2017 Rafał. All rights reserved.
//

import Foundation
import RxSwift

class MoreInfoCellViewModel {
    let title = Variable<String>("")
    let firstParameter = Variable<String>("")
    let firstDescription = Variable<String>("")
    let secondParameter = Variable<String>("")
    let secondDescription = Variable<String>("")

    let style: MoreInfoCellStyle

    public init(
            title: String,
            firstParameter: String,
            firstDescription: String,
            secondParameter: String,
            secondDescription: String,
            style: MoreInfoCellStyle) {

        self.title.value = title
        self.firstParameter.value = firstParameter
        self.firstDescription.value = firstDescription
        self.secondParameter.value = secondParameter
        self.secondDescription.value = secondDescription
        self.style = style
    }
}
