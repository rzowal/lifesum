//
// Created by Rafał on 04/08/2017.
// Copyright (c) 2017 Rafał. All rights reserved.
//

import Foundation
import UIKit

class MoreInfoDataSource: NSObject {

    var data: [MoreInfoCellViewModel] = []

    init(data: [MoreInfoCellViewModel]) {
        self.data = data
    }
}

extension MoreInfoDataSource: UICollectionViewDelegate {

}

extension MoreInfoDataSource: UICollectionViewDataSource {

    func collectionView(
            _ collectionView: UICollectionView,
            numberOfItemsInSection section: Int) -> Int {

        return data.count
    }

    public func collectionView(
            _ collectionView: UICollectionView,
            cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell: MoreInfoCell = collectionView.dequeueReusableCell(
                withReuseIdentifier: "MoreInfoCell",
                for: indexPath) as! MoreInfoCell

        cell.viewModel = data[indexPath.row]
        return cell
    }
}
