//
//  MoreInfoCellStyle.swift
//  HealthShake
//
//  Created by Rafał on 06/08/2017.
//  Copyright © 2017 Rafał. All rights reserved.
//

import Foundation
import UIKit

public struct MoreInfoCellStyle {

    public let gradientColours: [CGColor]
    public let backgroundColour: CGColor

    public init() {
        self.gradientColours = [Roundlayer.cgColorForRed(red: 243.0, green: 167.0, blue: 78.0),
                                Roundlayer.cgColorForRed(red: 237.0, green: 84.0, blue: 96.0)]
        self.backgroundColour = Roundlayer.cgColorForRed(red: 237.0, green: 84.0, blue: 96.0)
    }

    internal init(gradientColours: Roundlayer.GradientColour, backgroundColour: Roundlayer.GradientColour) {

        self.gradientColours = gradientColours.value
        self.backgroundColour = backgroundColour.backgroundValue
    }

}
