//
//  MoreInfoCell.swift
//  HealthShake
//
//  Created by Rafał on 04/08/2017.
//  Copyright © 2017 Rafał. All rights reserved.
//

import UIKit
import RxSwift

class MoreInfoCell: UICollectionViewCell {

    // Outlets
    @IBOutlet weak var foodNameLabel: UILabel!

    @IBOutlet weak var firstParameterLabel: UILabel!
    @IBOutlet weak var firstDescriptionLabel: UILabel!

    @IBOutlet weak var secondParameterLabel: UILabel!
    @IBOutlet weak var secondDescriptionLabel: UILabel!

    // Internal
    var reusableDisposeBag = DisposeBag()

    // Binding
    var viewModel: MoreInfoCellViewModel? {
        didSet {
            guard let viewModel = viewModel else {
                return
            }

            Roundlayer.setupGradientView(
                    view: self,
                    gradientColors: viewModel.style.gradientColours,
                    direction: [CGPoint(x: 0.0, y: 0.0), CGPoint(x: 1.0, y: 1.0)],
                    radius: 17,
                    shadowColor: viewModel.style.backgroundColour,
                    shadowOffset: CGSize(width: 0, height: 10),
                    shadowOpacity: 1.0,
                    blur: 20)

            viewModel
                    .title
                    .asObservable()
                    .map {
                        $0
                    }
                    .bind(to: self.foodNameLabel.rx.text)

            viewModel
                    .firstParameter
                    .asObservable()
                    .map {
                        $0
                    }
                    .bind(to: self.firstParameterLabel.rx.text)

            viewModel
                    .firstDescription
                    .asObservable()
                    .map {
                        $0
                    }
                    .bind(to: self.firstDescriptionLabel.rx.text)

            viewModel
                    .secondParameter
                    .asObservable()
                    .map {
                        $0
                    }
                    .bind(to: self.secondParameterLabel.rx.text)

            viewModel
                    .secondDescription
                    .asObservable()
                    .map {
                        $0
                    }
                    .bind(to: self.secondDescriptionLabel.rx.text)

        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        // Mandatory because insert Sublayers not update view
        if self.layer.sublayers != nil {
            self.layer.sublayers?.remove(at: 0)
        }
        reusableDisposeBag = DisposeBag()
    }
}
