//
//  RoundLayer.swift
//  HealthShake
//
//  Created by Rafał on 04/08/2017.
//  Copyright © 2017 Rafał. All rights reserved.
//

import Foundation
import UIKit

class Roundlayer {

    enum GradientColour {
        case green
        case purple
        case orange

        case backgroundGreen
        case backgroundPurple
        case backgroundOrange

        var value: [CGColor] {
            get {
                switch self {
                case .green:
                    return [Roundlayer.cgColorForRed(red: 75.0, green: 172.0, blue: 145.0),
                            Roundlayer.cgColorForRed(red: 106.0, green: 202.0, blue: 145.0)]

                case .purple:
                    return [Roundlayer.cgColorForRed(red: 114.0, green: 104.0, blue: 177.0),
                            Roundlayer.cgColorForRed(red: 114.0, green: 104.0, blue: 177.0)]

                case .orange:
                    return [Roundlayer.cgColorForRed(red: 231.0, green: 140.0, blue: 105.0),
                            Roundlayer.cgColorForRed(red: 236.0, green: 189.0, blue: 119.0)]

                default:
                    return [Roundlayer.cgColorForRed(red: 231.0, green: 140.0, blue: 105.0),
                            Roundlayer.cgColorForRed(red: 236.0, green: 189.0, blue: 119.0)]
                }
            }
        }

        var backgroundValue: CGColor {
            get {
                switch self {
                case .backgroundGreen:
                    return Roundlayer.cgColorForRed(red: 57.0, green: 190.0, blue: 128.0, alpha: 0.3)

                case .backgroundPurple:
                    return Roundlayer.cgColorForRed(red: 105.0, green: 92.0, blue: 173.0, alpha: 0.3)

                case .backgroundOrange:
                    return Roundlayer.cgColorForRed(red: 143.0, green: 177.0, blue: 96.0, alpha: 0.3)

                default:
                    return Roundlayer.cgColorForRed(red: 231.0, green: 140.0, blue: 105.0)
                }
            }
        }
    }

    public static func setupGradientView(
            view: UIView,
            gradientColors: [Any],
            direction: [CGPoint],
            radius: CGFloat = 0) {

        let gradientLayer = createGradient(
                view: view,
                gradientColors: gradientColors,
                direction: direction,
                radius: radius)

        view.layer.insertSublayer(gradientLayer, at: 0)
    }

    public static func setupGradientView(
            view: UIView,
            gradientColors: [Any],
            direction: [CGPoint],
            radius: CGFloat = 0,
            shadowColor: CGColor,
            shadowOffset: CGSize,
            shadowOpacity: Float,
            blur: CGFloat) {

        let gradientLayer = createGradient(
                view: view,
                gradientColors: gradientColors,
                direction: direction,
                radius: radius)

        gradientLayer.shadowColor = shadowColor
        gradientLayer.shadowOpacity = shadowOpacity
        gradientLayer.shadowOffset = shadowOffset
        gradientLayer.shadowRadius = blur
        gradientLayer.opacity = 1.0

        view.layer.insertSublayer(gradientLayer, at: 0)
    }

    private static func createGradient(
            view: UIView,
            gradientColors: [Any],
            direction: [CGPoint],
            radius: CGFloat = 0) -> CAGradientLayer {

        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = view.bounds
        gradientLayer.colors = gradientColors

        gradientLayer.startPoint = direction[0]
        gradientLayer.endPoint = direction[1]
        gradientLayer.masksToBounds = false

        gradientLayer.opacity = 1.0
        gradientLayer.masksToBounds = false

        gradientLayer.cornerRadius = radius
        gradientLayer.shouldRasterize = true

        return gradientLayer
    }

    public static func cgColorForRed(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat = 1) -> CGColor {
        return UIColor(red: red / 255.0, green: green / 255.0, blue: blue / 255.0, alpha: alpha).cgColor
    }
}
