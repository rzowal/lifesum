//
// Created by Rafał on 31/07/2017.
// Copyright (c) 2017 Rafał. All rights reserved.
//

import Foundation

// MARK: Definition for Result

enum Result<T> {
    case success(T)
    case error(Error)

    init(value: T) {
        self = .success(value)
    }

    init(error: Error) {
        self = .error(error)
    }

    init(_ value: T?, failWith: ((Void) -> (Error))) {
        self = value.map(Result.success) ?? .error(failWith())
    }

    func analysis<U>(ifSuccess: (T) -> U, ifFailure: (Error) -> U) -> U {
        switch self {
        case let .success(value): return ifSuccess(value)
        case let .error(value): return ifFailure(value)
        }
    }
}

// MARK: Success / Failure

extension Result {
    var isSuccess: Bool {
        return analysis(ifSuccess: { _ in true }, ifFailure: { _ in false })
    }

    var isFailure: Bool {
        return !isSuccess
    }
}

// MARK: Value / Error

extension Result {
    var value: T? {
        return analysis(ifSuccess: { $0 }, ifFailure: { _ in nil })
    }

    var error: Error? {
        return analysis(ifSuccess: { _ in nil }, ifFailure: { $0 })
    }
}

// MARK: Recovering

// Returns the value of `left` if it is a `Success`, or `right` otherwise. Short-circuits.
func ?? <T>(left: Result<T>, right: @autoclosure () -> T) -> T {
    return left.recover(right())
}

// Returns `left` if it is a `Success`es, or `right` otherwise. Short-circuits.
func ?? <T>(left: Result<T>, right: @autoclosure () -> Result<T>) -> Result<T> {
    return left.recover(with: right())
}
