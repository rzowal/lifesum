//
// Created by Rafał on 31/07/2017.
// Copyright (c) 2017 Rafał. All rights reserved.
//

import Foundation

extension Result {
    // Returns `self.value` if this result is a .Success, or the given value otherwise. Equivalent with `??`
    func recover(_ value: @autoclosure () -> T) -> T {
        return self.value ?? value()
    }

    // Returns this result if it is a .Success, or the given result otherwise. Equivalent with `??`
    public func recover(with result: @autoclosure () -> Result<T>) -> Result<T> {
        return analysis(
                ifSuccess: { _ in self },
                ifFailure: { _ in result() }
        )
    }
}
