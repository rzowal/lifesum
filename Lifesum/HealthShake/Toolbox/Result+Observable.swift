//
// Created by Rafał on 31/07/2017.
// Copyright (c) 2017 Rafał. All rights reserved.
//

import Foundation
import RxSwift

extension Observable {
    func asResult() -> Observable<Result<E>> {
        return self
                .map {
                    Result.success($0)
                }
                .catchError { error -> Observable<Result<E>> in
                    return Observable<Result<E>>.just(Result<E>.error(error))
                }
    }
}
