//
//  FoodipediaTests.swift
//  HealthShake
//
//  Created by Rafał on 07/08/2017.
//  Copyright © 2017 Rafał. All rights reserved.
//

import XCTest
import EarlGrey
import Nimble

import LifesumStubWoofer
import NetworkServices

@testable import HealthShake

class FoodipediaTests: XCTestCase {

    override func setUp() {
        super.setUp()

        StubWoofer.stubFoodipedia(response: .validFirstFood)
        window.makeKeyAndVisible()
    }

    override func tearDown() {
        self.tearDownHierarchy()
        StubWoofer.removeAll()
        super.tearDown()
    }

    func testFoodipediaMoreInfoView() {
        given("User is on Home View") {

        }

        when("User tap start button") {
            EarlGrey.select(elementWithMatcher: grey_accessibilityID(
                            HomeViewController.AccessibilityID.startButton.value))
                    .perform(grey_tap())
        }

        and("User tap start shake phone") {
            EarlGrey.select(elementWithMatcher: grey_accessibilityID(
                            FoodipediaViewController.AccessibilityID.view.value))
                    .assert(with: grey_sufficientlyVisible())
        }

        and("User tap moreInfo View") {
            EarlGrey.select(elementWithMatcher: grey_accessibilityID(
                    FoodipediaViewController.AccessibilityID.moreInfoButton.value))
                    .perform(grey_tap())
        }

        then("Should see More Info") {
            EarlGrey.select(elementWithMatcher: grey_accessibilityID(
                            MoreInfoViewController.AccessibilityID.view.value))
                    .assert(with: grey_sufficientlyVisible())
        }
    }
}
