//
//  XCTestCase+Window.swift
//  DAZN
//
//  Created by Rafal on 28/03/2017.
//  Copyright © 2017 DAZN. All rights reserved.
//

import XCTest
@testable import HealthShake

extension XCTestCase {
    var window: UIWindow {
        //swiftlint:disable force_cast
        return (UIApplication.shared.delegate as! AppDelegate).window!
        //swiftlint:enable force_cast
    }

    func tearDownHierarchy() {
        self.window.rootViewController?.dismiss(animated: false, completion: nil)
        self.window.rootViewController = UIViewController()
    }
}
