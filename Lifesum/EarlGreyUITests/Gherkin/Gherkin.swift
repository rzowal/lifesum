//
//  Gherkin.swift
//  HealthShake
//
//  Created by Rafał on 07/08/2017.
//  Copyright © 2017 Rafał. All rights reserved.
//

import Foundation

public protocol Gherkin {
    func given(_ description: String, _ closure: () -> Void)
    func when(_ description: String, _ closure: () -> Void)
    func and(_ description: String, _ closure: () -> Void)
    func then(_ description: String, _ closure: () -> Void)
}
