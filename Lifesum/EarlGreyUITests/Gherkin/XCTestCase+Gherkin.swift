//
//  XCTestCase+Gherkin.swift
//  HealthShake
//
//  Created by Rafał on 07/08/2017.
//  Copyright © 2017 Rafał. All rights reserved.
//

import XCTest

extension Gherkin where Self: XCTestCase {
    
    public func background(_ description: String, _ closure: () -> Void) {
        
    }
    
    public func scenario(_ description: String, _ closure: () -> Void) {
        closure()
    }
    
    public func scenario(_ closure: () -> Void) {
        closure()
    }
    
    public func given(_ description: String, _ closure: () -> Void) {
        closure()
    }
    
    public func when(_ description: String, _ closure: () -> Void) {
        closure()
    }
    
    public func and(_ description: String, _ closure: () -> Void) {
        closure()
    }
    
    public func then(_ description: String, _ closure: () -> Void) {
        closure()
    }
}

extension XCTestCase: Gherkin {
    
}
