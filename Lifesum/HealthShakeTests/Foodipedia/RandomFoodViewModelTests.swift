//
//  RandomFoodViewModelTests.swift
//  HealthShake
//
//  Created by Rafał on 03/08/2017.
//  Copyright © 2017 Rafał. All rights reserved.
//

import XCTest
import Nimble
import RxSwift
import RxBlocking
import LifesumApi
import LifesumStubWoofer
import NetworkServices

@testable import HealthShake

class FoodipediaViewModelTests: XCTestCase {

    let disposeBag = DisposeBag()

    func testFoodipediaViewModel() {

        StubWoofer.stubFoodipedia(response: .validFirstFood)

        var foodipediaResponse: Result<FoodipediaResponse>?
        let networkServices = NetworkServices()
        let foodipediaViewModel = FoodipediaViewModel(networkServices: networkServices)

//        do {
//            foodipediaResponse = try foodipediaViewModel
//                    .outputs
//                    .onResultFoodipediaRequest
//                    .asObservable()
//                    .toBlocking()
//                    .single()
//
//        } catch {
//            fail("We dont' expect that")
//        }

        foodipediaViewModel.inputs.randomFood()

//        expect(foodipediaResponse?.value?.response.title).to(equal("Ricotta cheese"))
    }
}
