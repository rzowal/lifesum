//
// Created by Rafał on 31/07/2017.
// Copyright (c) 2017 Rafał. All rights reserved.
//

import Foundation
import OHHTTPStubs

extension StubWoofer {

    public enum FoodipediaResponse {
        case validFirstFood
        case validLastFood
        case invalidJSON
    }

    public class func stubFoodipedia(response: FoodipediaResponse) {
        self.lifesumDescriptor = stub(
                condition: pathContains("foodipedia"),
                response: {
                    _ in
                    let fileName: String
                    let code: Int32

                    switch response {
                    case .validFirstFood:
                        fileName = "ValidFirstFood.json"
                        code = 200
                    case .validLastFood:
                        fileName = "ValidLastFood.json"
                        code = 200
                    case .invalidJSON:
                        fileName = "Invalid.json"
                        code = 200
                    }

                    let data = OHPathForFileInBundle(fileName, StubWoofer.stubBundle)
                    return fixture(filePath: data!, status: code, headers: nil)
                })
    }
}
