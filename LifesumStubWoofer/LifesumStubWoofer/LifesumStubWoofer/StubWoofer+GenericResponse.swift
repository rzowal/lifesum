//
// Created by Rafał on 31/07/2017.
// Copyright (c) 2017 Rafał. All rights reserved.
//

import Foundation
import OHHTTPStubs

extension StubWoofer {
    public enum GenericError {
        case noInternetConnection
        case invalidJSON
    }

    class func genericStub(genericResult: GenericError) -> OHHTTPStubsResponse {
        let file: String
        let code: Int32

        switch genericResult {
        case .noInternetConnection:
            let errorCode = CFNetworkErrors.cfurlErrorNotConnectedToInternet.rawValue
            let notConnectedError = NSError(
                    domain: NSURLErrorDomain,
                    code: Int(errorCode), userInfo: nil)
            return OHHTTPStubsResponse(error: notConnectedError)
        case .invalidJSON:
            file = "Invalid.json"
            code = 200
        }
        let data = OHPathForFileInBundle(file, StubWoofer.stubBundle)
        return fixture(filePath: data!, status: code, headers: nil)
    }
}