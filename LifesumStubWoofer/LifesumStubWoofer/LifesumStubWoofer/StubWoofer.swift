//
// Created by Rafał on 31/07/2017.
// Copyright (c) 2017 Rafał. All rights reserved.
//

import Foundation
import OHHTTPStubs
import ObjectMapper

public func pathEndsWith(_ path: String) -> OHHTTPStubsTestBlock {
    return {
        req in
        req.url?.path.hasSuffix(path) ?? false
    }
}

public func pathContains(_ path: String) -> OHHTTPStubsTestBlock {
    return {
        req in
        req.url?.path.contains(path) ?? false
    }
}

public class StubWoofer {
    public enum WooferError: Error {
        case loadBundle(String)
        case findResource(String)
        case loadData(String)
        case loadJSONDict(String)
        case loadString(String)
        case createMappable(String)
    }

    static var lifesumDescriptor: OHHTTPStubsDescriptor?

    public static func removeAll() {
        if let stub = lifesumDescriptor {
            OHHTTPStubs.removeStub(stub)
            lifesumDescriptor = nil
        }
    }

    public static func remove(_ descriptor: OHHTTPStubsDescriptor) {
        OHHTTPStubs.removeStub(descriptor)
    }

    static var stubBundle: Bundle {
        let bundle = Bundle(for: StubWoofer.self)
        return bundle
    }

    public class func enableStubs(_ enable: Bool) {
        OHHTTPStubs.setEnabled(enable)
    }

    public class func loadJSON(name: String, withExtension: String? = "json") -> [String: Any]? {
        let bundle = Bundle(identifier: "com.lifesum.LifesumStubWoofer")
        guard let url = bundle?.url(forResource: name, withExtension: withExtension) else {
            print("Failed to load file \(name)")
            return nil
        }

        guard let data = try? Data(contentsOf: url) else {
            print("Unable to get contents of url")
            return nil
        }
        let json = try? JSONSerialization.jsonObject(
                with: data,
                options: .allowFragments)
        return json as? [String: Any]
    }

    private class func jsonFromFile(name: String, withExtension: String? = "json") -> Any? {
        let bundle = Bundle(identifier: "com.lifesum.LifesumStubWoofer")
        guard let url = bundle?.url(forResource: name, withExtension: withExtension) else {
            print("Failed to load file \(name)")
            return nil
        }

        guard let data = try? Data(contentsOf: url) else {
            print("Unable to get contents of url")
            return nil
        }

        return try? JSONSerialization.jsonObject(
                with: data,
                options: .allowFragments)
    }

    public class func loadJSON<T:Mappable>(name: String, withExtension: String? = "json") -> T? {

        guard let json = jsonFromFile(name: name, withExtension: withExtension ) else {
            print("Unable to get contents of url")
            return nil
        }

        guard let mappedResponse = Mapper<T>().map(JSONObject: json) else {
            print("Failed to map JSON")
            return nil
        }
        return mappedResponse
    }

//
//    public class func loadJSON<T:ImmutableMappable>(name: String, withExtension: String? = "json") -> T? {
//        guard let json = jsonFromFile(name: name, withExtension: withExtension ) else {
//            print("Unable to get contents of url")
//            return nil
//        }
//        let mappedResponse = try? T(JSONObject: json)
//        return mappedResponse
//    }

    public class func pathForFile(name: String) throws -> String {
        let bundle = Bundle(for: self)

        guard let path = bundle.path(forResource: "JSONs/\(name)", ofType: nil) else {
            throw WooferError.findResource(name)
        }

        return path
    }

    public class func loadData(name: String) throws -> Data {

        let bundle = Bundle(for: self)

        guard let url = bundle.url(forResource: "JSONs/\(name)", withExtension: nil) else {
            throw WooferError.findResource(name)
        }

        let data = try Data(contentsOf: url)

        return data
    }

    public class func loadString(name: String) throws -> String {

        let data: Data = try self.loadData(name: name)

        guard let string = String(data: data, encoding: .utf8) else {
            throw WooferError.loadString(name)
        }

        return string
    }

    public class func loadAny(name: String) throws -> Any {

        let data: Data = try self.loadData(name: name)

        let object = try JSONSerialization.jsonObject(with: data)

        return object
    }

    public class func load(name: String) throws -> [String: Any] {

        guard let dict = try self.loadAny(name: name) as? [String: Any] else {
            throw WooferError.loadJSONDict(name)
        }

        return dict
    }

    public class func loadData(url: URL) throws -> Data {

        let data = try Data(contentsOf: url)

        return data
    }

    public class func url(fileInJSONs: String) throws -> URL {

        let bundle = Bundle(for: self)

        guard let url = bundle.url(forResource: "JSONs/\(fileInJSONs)", withExtension: nil) else {
            throw WooferError.findResource(fileInJSONs)
        }

        return url
    }

    public class func jsonMock(
            file: String) throws
                    -> (url: URL, data: Data) {

        let url = try self.url(fileInJSONs: file)
        let data = try self.loadData(url: url)
        return (url, data)
    }

    public class func object<T:Mappable>(file: String) throws
                    -> T {

        let mock = try self.jsonMock(file: file)
        let json = try? JSONSerialization.jsonObject(
                with: mock.data,
                options: .allowFragments)

        guard let object = Mapper<T>().map(JSONObject: json) else {
            throw WooferError.createMappable(file)
        }

        return object
    }
//
//    public class func object<T:ImmutableMappable>(file: String) throws
//                    -> T {
//
//        let mock = try self.jsonMock(file: file)
//        let json = try? JSONSerialization.jsonObject(
//                with: mock.data,
//                options: .allowFragments)
//
//        let object = try T.init(JSONObject: json)
//
//        return object
//    }
}
