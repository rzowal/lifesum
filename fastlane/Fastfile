default_platform :ios

platform :ios do

  default_device = "iPhone 6s"

  devices_tests_ab = [
      "iPhone 6s",
      “iPhone 7”
  ]

  schemes_tests_ab = [
      "ABTests"
  ]

  schemes_tests_ui = [
      "EarlGreyUITests"
  ]

  schemes_tests = [
      “LifesumApi”,
      “NetworkService”
  ]

 desc "Install all dependencies"
  lane :make do
    #carthage(
    #    command: "bootstrap",
    #    platform: "iOS",
    #    use_binaries: true)
    sh("cd .. && carthage bootstrap --platform iOS --cache-builds")
  end

  desc "Run Release Tests"
  lane :test_release do
    test
    test_ab
    test_ui
  end

  desc "Run all AB Tests"
  lane :test_ab do
    devices = devices_tests_ab
    schemes = schemes_tests_ab

    schemes.each do |scheme|
      test_generic(
          scheme: scheme,
          devices: devices)
    end
  end

  desc "Run UI tests"
  lane :test_ui do
    schemes_tests_ui.each do |scheme|
      test_generic(
          scheme: scheme,
          devices: [default_device])
    end
  end

  desc "Run unit tests"
  lane :test do
    schemes_tests.each do |scheme|
      test_generic(
          scheme: scheme,
          devices: [default_device])
    end
  end
  
  desc "Runs unit tests for selected scheme"
  lane :test_generic do |options|

    scheme = options[:scheme]
    devices = options[:devices]

    scan(
        scheme: scheme,
        code_coverage: true,
        devices: devices,
        derived_data_path: "./build/#{scheme}.DerivedData",
        output_directory: "./build/#{scheme}.reports/",
        formatter: 'xcpretty-json-formatter')
  end
end
