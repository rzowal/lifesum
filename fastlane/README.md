fastlane documentation
================
# Installation
```
sudo gem install fastlane
```
# Available Actions
## iOS
### ios make
```
fastlane ios make
```
Install all dependencies
### ios test_release
```
fastlane ios test_release
```
Run Release Tests
### ios test_ab
```
fastlane ios test_ab
```
Run all AB Tests
### ios test_ui
```
fastlane ios test_ui
```
Run UI tests
### ios test
```
fastlane ios test
```
Run unit tests
### ios test_generic
```
fastlane ios test_generic
```
Runs unit tests for selected scheme

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [https://fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [GitHub](https://github.com/fastlane/fastlane/tree/master/fastlane).
